$(function() {
	$("#btnWyslij").click(function() {
		var $frm = $("#formZapytanie");

		$.post($frm.attr('action'), $frm.serializeArray(), function(resp) {
			if(resp == 'ok') {
				alert("Dziękujemy za wysłanie zapytania.");
				$("textarea").val('');
				$('#modalZapytanie').modal('hide');
			} else {
				alert("Wystąpił błąd");
			}
		});
		
		return false;
	});

    $("#btnWyslijOferte").click(function() {
		var $frm = $("#formWyslijOferte");

		$.post($frm.attr('action'), $frm.serializeArray(), function(resp) {
			if(resp == 'ok') {
				alert("Oferta zostanie niebawem wysłana.");
				$("textarea").val('');
				$('#modalWyslijOferte').modal('hide');
			} else {
				alert("Wystąpił błąd");
			}
		});

		return false;
	});
});