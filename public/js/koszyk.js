$(function() {
	$(".usunZKoszyka").click(function() {

		 $link = $(this);
		var url = $(this).attr('href');
		var dodano = "<span class='glyphicon glyphicon-ok' style='color: green'></span>";
		
		$.post(url, function(resp) {
            var odp = JSON.parse(resp);
			if(odp.success){
                $link.replaceWith(dodano);
                $('.glyphicon-ok').parent().parent().remove();
                $('#liczba_ofert').html(odp.koszyk_liczba_ofert);
            }
			else
				alert('Wystąpił błąd');
		});
		
		return false;
	});
});