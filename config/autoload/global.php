<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
	'db' => array(
		'driver' => 'Pdo',
		'dsn' => 'mysql:dbname=pai;host=localhost',
		'driver_options' => array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
		),
		'username' => 'root',
		'password' => '',
	),
	'service_manager' => array(
		'factories' => array(
			'db_adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
		),
	),
    'mail' => array(
        'name' => 'imap.wit.edu.pl',
        'host' => 'imap.wit.edu.pl',
        'port' => 465,
        'connection_class' => 'login',
        'connection_config' => array(
            'username' => 'patoka',
            'password' => '',
            'ssl' => 'ssl'
        ),
        'from' => array(
            'email' => 'patoka@wit.edu.pl',
            'name' => 'Administrator strony'
        )
    ),
);
