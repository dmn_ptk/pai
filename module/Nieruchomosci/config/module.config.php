<?php
return array(
    'router' => array(
        'routes' => array(
            'nieruchomosci' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Nieruchomosci\Controller',
                        'controller' => 'Oferty',
                        'action' => 'lista',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'oferty' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'oferty[/:action][/:id]',
                        ),
                    ),
                    'oferty-lista' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => 'oferty/lista[/strona/:strona]',
                        ),
                    )
                ),
            ),
            'koszyk' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/koszyk[/:action][/:id]',
                    'defaults' => array(
                        'controller'    => 'Nieruchomosci\Controller\Koszyk',
                        'action'        => 'lista',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'lista' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => 'lista',
                            'defaults' => array(
                                'controller' => 'Nieruchomosci\Controller\Koszyk',
                                'action' => 'lista'
                            )
                        ),
                    ),
//                    'usuń' => array(
//                        'type' => 'Segment',
//                        'options' => array(
//                            'route' => 'usun[/:id]',
//                            'defaults' => array(
//                                'controller' => 'Nieruchomosci\Controller\Koszyk',
//                                'action' => 'usun'
//                            )
//                        ),
//                    ),
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Nieruchomosci\Controller\Oferty' => Nieruchomosci\Controller\OfertyController::class,
            'Nieruchomosci\Controller\Koszyk' => Nieruchomosci\Controller\KoszykController::class,

        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'navigation' => Zend\Navigation\Service\DefaultNavigationFactory::class,
        ),
        'invokables' => array(
            'Nieruchomosci\Model\Oferta' => Nieruchomosci\Model\Oferta::class,
            'Nieruchomosci\Model\Koszyk' => Nieruchomosci\Model\Koszyk::class,
        ),
        'aliases' => array(
            'Oferta' => 'Nieruchomosci\Model\Oferta',
            'Koszyk' => 'Nieruchomosci\Model\Koszyk',
        )
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/nieruchomosci' => __DIR__ . '/../view/layout/nieruchomosci.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Oferty',
                'route' => 'nieruchomosci',
                'action' => 'lista'
            ),
            array(
                'label' => 'Koszyk',
                'route' => 'koszyk',
            )
        ),
    ),
);