<?php
namespace Nieruchomosci;

use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\InitProviderInterface;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface, InitProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Zapytanie' => function($sm) {
                    $config = $sm->get('config');
                    $zapytanie = new \Nieruchomosci\Model\Zapytanie($config['mail']);

                    return $zapytanie;
                },
                'LogZapytan' => function($sm) {
                    $log = new \Nieruchomosci\Service\LogZapytanOfertowych($sm->get('db_adapter'));

                    return $log;
                },
            ),
        );
    }

    public function init(\Zend\ModuleManager\ModuleManagerInterface $manager)
    {
        $manager->getEventManager()->getSharedManager()->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, function(MvcEvent $e) {
            $e->getTarget()->layout('layout/nieruchomosci');
            $e->getViewModel()->liczba_ofert = $e->getApplication()->getServiceManager()->get('Koszyk')->liczbaOfert();
        });
    }

    /**
     * @param  \Zend\EventManager\EventInterface  $e The MvcEvent
     * @return void
     */
    public function onBootstrap(\Zend\EventManager\EventInterface $e)
    {
        // uruchom sesje
        $session = new \Zend\Session\SessionManager();
        $session->start();
    }
}