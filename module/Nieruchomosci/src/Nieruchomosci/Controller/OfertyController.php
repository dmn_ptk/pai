<?php

namespace Nieruchomosci\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Nieruchomosci\Form;
use DOMPDFModule\View\Model\PdfModel;

class OfertyController extends AbstractActionController
{
    public function listaAction()
    {
        $parametry = $this->params()->fromQuery();
        $strona = $this->params()->fromRoute('strona', 1);

        // pobierz dane ofert
        $oferta = $this->getServiceLocator()->get('Oferta');
        $koszyk = $this->getServiceLocator()->get('Koszyk');
        $paginator = $oferta->pobierzWszystko($parametry);
        $paginator->setItemCountPerPage(10)->setCurrentPageNumber($strona);

        // zbuduj formularz wyszukiwania
        $form = new Form\OfertaSzukajForm();
        $form->populateValues($parametry);

        return new ViewModel(array(
            'form' => $form,
            'oferty' => $paginator,
            'idki_ofert_w_koszyku' => $koszyk->pobierzIdkiZawartosci(),
            'parametry' => $parametry
        ));
    }

    public function szczegolyAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id))
            $this->redirect()->toRoute('oferty');

        $oferta = $this->getServiceLocator()->get('Oferta');
        $dane = $oferta->pobierz($id);
        $viewModel = new ViewModel(array('tytul' => 'Szczegóły nieruchomości', 'dane' => $dane));
        return $viewModel;
    }

//    public function szczegolyAction()
//    {
//        $oferta = $this->getServiceLocator()->get('Oferta');
//        $daneOferty = $oferta->pobierz($this->params()->fromRoute('id'));
//
//        return array('oferta' => $daneOferty);
//    }

    public function koszykDodajAction()
    {
        $koszyk = $this->getServiceLocator()->get('Koszyk');
        $koszyk->dodaj($this->params()->fromRoute('id'));

        $result = array('success' => true, 'koszyk_liczba_ofert' => $koszyk->liczbaOfert());
        echo json_encode($result);
        return $this->getResponse();
    }

    public function drukujAction()
    {
        $oferta = $this->getServiceLocator()->get('Oferta');
        $daneOferty = $oferta->pobierz($this->params()->fromRoute('id'));

        $pdf = new PdfModel();
        $pdf->setOption('filename', 'oferta_' . $daneOferty->numer);
        $pdf->setOption('paperSize', 'a4');
        $pdf->setOption('paperOrientation', 'portrait');
        $pdf->setTerminal(true);

        $pdf->setVariables(array(
            'oferta' => $daneOferty
        ));

        return $pdf;
    }


    public function wyslijZapytanieAction()
    {
        $id = $this->params()->fromRoute('id');
        if($this->getRequest()->isPost() && $id) {
            $daneOferty = $this->getServiceLocator()->get('Oferta')->pobierz($id);
            $zapytanie = $this->getServiceLocator()->get('Zapytanie');
            $params = $this->params()->fromPost();
            $wynik = $zapytanie->wyslij($daneOferty, $params);

            $logZapytan = $this->getServiceLocator()->get('LogZapytan');
            $logZapytan->zapiszZapytanie($params);

            if($wynik)
                echo 'ok';
        }

        return $this->getResponse();
    }

    public function wyslijOferteAction()
    {

        $id = $this->params()->fromRoute('id');
        $email = $this->params()->fromPost('email');
        if($this->getRequest()->isPost() && $id) {
            $daneOferty = $this->getServiceLocator()->get('Oferta')->pobierz($id);
            $zapytanie = $this->getServiceLocator()->get('Zapytanie');
            $wynik = $zapytanie->wyslijOferte($daneOferty, $email);

            if($wynik)
                echo 'ok';
        }

        return $this->getResponse();
    }
}