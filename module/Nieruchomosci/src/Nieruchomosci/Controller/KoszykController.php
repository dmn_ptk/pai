<?php

namespace Nieruchomosci\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Nieruchomosci\Form;
use DOMPDFModule\View\Model\PdfModel;

class KoszykController extends AbstractActionController
{
    public function listaAction()
    {
        // pobierz dane ofert
        $koszyk = $this->getServiceLocator()->get('Koszyk');

        // zbuduj formularz wyszukiwania
        $form = new Form\OfertaSzukajForm();

        return new ViewModel(array(
            'wybrane' => $koszyk->pobierzZawartosc(),
            'form' => $form,
        ));
    }

    public function usunAction()
    {
        $koszyk = $this->getServiceLocator()->get('Koszyk');
        $koszyk->usun($this->params()->fromRoute('id'));

        $result = array('success' => true, 'koszyk_liczba_ofert' => $koszyk->liczbaOfert());
        echo json_encode($result);
        return $this->getResponse();
    }

    public function drukujAction()
    {
        $oferta = $this->getServiceLocator()->get('Oferta');
        $daneOferty = $oferta->pobierz($this->params()->fromRoute('id'));

        $pdf = new PdfModel();
        $pdf->setOption('filename', 'oferta_' . $daneOferty->numer);
        $pdf->setOption('paperSize', 'a4');
        $pdf->setOption('paperOrientation', 'portrait');
        $pdf->setTerminal(true);

        $pdf->setVariables(array(
            'oferta' => $daneOferty
        ));

        return $pdf;
    }

    public function drukujKoszykAction()
    {

        $koszyk = $this->getServiceLocator()->get('Koszyk');
        $zawartoscKoszyka = $koszyk->pobierzZawartosc();

        $pdf = new PdfModel();
        $pdf->setOption('filename', 'koszyk_' . date('Y-m-d'));
        $pdf->setOption('paperSize', 'a4');
        $pdf->setOption('paperOrientation', 'portrait');
        $pdf->setTerminal(true);

        $pdf->setVariables(array(
            'wybrane' => $zawartoscKoszyka
        ));

        return $pdf;
    }
}