<?php

namespace Nieruchomosci\Model;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Db\Sql\Sql;
use Zend\Session\Container;
use Zend\Db\Adapter as DbAdapter;

class Koszyk implements DbAdapter\AdapterAwareInterface
{
    use DbAdapter\AdapterAwareTrait;

    protected $sesja;

    public function __construct()
    {
        $this->sesja = new Container('koszyk');
        $this->sesja->liczba_ofert = $this->sesja->liczba_ofert ? $this->sesja->liczba_ofert : 0;
    }

    public function dodaj($idOferty)
    {
        $dbAdapter = $this->adapter;
        $session = new \Zend\Session\SessionManager();

        $sql = new Sql($dbAdapter);
        $insert = $sql->insert('koszyk');
        $insert->values(array(
            'id_oferty' => $idOferty,
            'id_sesji' => $session->getId()
        ));

        $selectString = $sql->getSqlStringForSqlObject($insert);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        $this->sesja->liczba_ofert++;

        try {
            return $wynik->getGeneratedValue();
        } catch (Exception $e) {
            return false;
        }
    }

    public function usun($idOferty)
    {
        $dbAdapter = $this->adapter;
        $session = new \Zend\Session\SessionManager();

        $sql = new Sql($dbAdapter);
        $delete = $sql->delete('koszyk');
        $delete->where(array(
                'id_oferty' => $idOferty,
                'id_sesji' => $session->getId()
            ));


        $deleteString = $sql->getSqlStringForSqlObject($delete);
        $wynik = $dbAdapter->query($deleteString, $dbAdapter::QUERY_MODE_EXECUTE);
    }

    public function liczbaOfert()
    {
//        return $this->sesja->liczba_ofert;

        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $select = $sql->select('koszyk');
        $select->where(array('id_sesji' => session_id()));

        $selectString = $sql->getSqlStringForSqlObject($select);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        return $wynik->count();
    }

    /**
     * Zwaraca jednowymiarowy array z ID ofert w koszyku
     * @return array
     */
    public function pobierzIdkiZawartosci()
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $select = $sql->select('koszyk');
        $select->where(array('id_sesji' => session_id()));

        $selectString = $sql->getSqlStringForSqlObject($select);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        $idki = array();
        if ($wynik->count() > 0)
            while ($wynik->valid()) {
                $idki[] = $wynik->current()->id_oferty;
                $wynik->next();
            }

        return $idki;
    }

    /**
     * Pobiera zawartosc koszyka
     * @return DbAdapter\Driver\StatementInterface|\Zend\Db\ResultSet\ResultSet
     */
    public function pobierzZawartosc()
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $select = $sql->select(array('k' => 'koszyk'));
        $select->join(array('o' => 'oferty'), 'o.id = k.id_oferty');
        $select->where(array('id_sesji' => session_id()));

        $selectString = $sql->getSqlStringForSqlObject($select);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        return $wynik;
    }
}