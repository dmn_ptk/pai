<?php

namespace Nieruchomosci\Model;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Adapter as DbAdapter;

class Oferta implements DbAdapter\AdapterAwareInterface
{
    use DbAdapter\AdapterAwareTrait;

    /**
     * Pobiera obiekt Paginator dla przekazanych parametrów.
     *
     * @param array $szukaj
     * @return \Zend\Paginator\Paginator
     */
    public function pobierzWszystko($szukaj = array())
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $select = $sql->select('oferty');
        $platform = $dbAdapter->getPlatform();

        if(!empty($szukaj['typ_oferty']))
            $select->where(array('typ_oferty' => $szukaj['typ_oferty']));
        if(!empty($szukaj['typ_nieruchomosci']))
            $select->where(array('typ_nieruchomosci' => $szukaj['typ_nieruchomosci']));
        if(!empty($szukaj['numer']))
            $select->where(array('numer' => $szukaj['numer']));
        if(!empty($szukaj['powierzchnia_od']))
            $select->where('powierzchnia >= ' . $platform->quoteValue($szukaj['powierzchnia_od']));
        if(!empty($szukaj['powierzchnia_do']))
            $select->where(array('powierzchnia <= ' . $platform->quoteValue( $szukaj['powierzchnia_do'])));
        if(!empty($szukaj['cena_od']))
            $select->where('cena >= ' . $platform->quoteValue($szukaj['cena_od']));
        if(!empty($szukaj['cena_do']))
            $select->where('cena <= ' . $platform->quoteValue($szukaj['cena_do']));

        $paginatorAdapter = new DbSelect($select, $dbAdapter);
        $paginator = new Paginator($paginatorAdapter);

        return $paginator;
    }

    public function pobierz($id)
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $select = $sql->select('oferty');
        $select->where(array('id' => $id));

        $selectString = $sql->getSqlStringForSqlObject($select);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        if($wynik->count())
            return $wynik->current();
        else
            return array();
    }
}