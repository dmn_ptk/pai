<?php

namespace Nieruchomosci\Model;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class Zapytanie
{
	private $smtpTransportConfig;
	private $from;
	
	public function __construct(array $config)
	{
		$this->from = $config['from'];
		unset($config['from']);
		
		$this->smtpTransportConfig = $config;
	}
	
	public function wyslij($daneOferty, $params)
	{
		$transport = new SmtpTransport();
		$options = new SmtpOptions($this->smtpTransportConfig);
		$transport->setOptions($options);

        $emailContent = "Klient wyraził zainteresowanie ofertą numer *$daneOferty[numer]* o treści:\n\n$params[tresc]\n\n";
        $emailContent .= "Dane kontaktowe\n\nTelefon: $params[telefon]\nEmail: $params[email]";
		$part = new MimePart($emailContent);
		$part->type = 'text/plain';
		$part->charset = 'utf-8';
		
		$body = new MimeMessage();
		$body->setParts([$part]);
		
		$message = new Message();
		$message->setEncoding('UTF-8');
		$message->setFrom($this->from['email'], $this->from['name']); // konto do wysyłania maili z serwisu
		$message->addTo('patoka@wit.edu.pl', "Odbiorca"); // osoba obsługująca zgłoszenia
		$message->setSubject("Zainteresowanie ofertą");
		$message->setBody($body);

		try {
			$transport->send($message);

			return true;
		} catch(\Exception $e) {
			return false;
		}
	}

	public function wyslijOferte($daneOferty, $email)
	{
		$transport = new SmtpTransport();
		$options = new SmtpOptions($this->smtpTransportConfig);
		$transport->setOptions($options);

        $emailContent = "Dane oferty *$daneOferty[numer]*\n\n";
        $emailContent .= "typ oferty: $daneOferty[typ_oferty]\n\n";
        $emailContent .= "typ nieruchomości: $daneOferty[typ_nieruchomosci]\n\n";
        $emailContent .= "powierzchnia: $daneOferty[powierzchnia]\n\n";
        $emailContent .= "cena: $daneOferty[cena] zł";
		$part = new MimePart($emailContent);
		$part->type = 'text/plain';
		$part->charset = 'utf-8';

		$body = new MimeMessage();
		$body->setParts([$part]);

		$message = new Message();
		$message->setEncoding('UTF-8');
		$message->setFrom($this->from['email'], $this->from['name']); // konto do wysyłania maili z serwisu
		$message->addTo($email, "Odbiorca");
		$message->setSubject("Informacje na temat oferty");
		$message->setBody($body);

		try {
			$transport->send($message);

			return true;
		} catch(\Exception $e) {
			return false;
		}
	}
}