<?php

namespace Nieruchomosci\Form;

use Zend\Form\Form;

class OfertaSzukajForm extends Form
{
    /**
     * Ustaw dodatkowe właściwości formularza.
     */
    public function przygotujWyglad()
    {
        foreach($this->getElements() as $elem) {
            if(!$elem instanceof \Zend\Form\Element\Submit)
                $elem->setAttribute('class', 'form-control');
        }
    }

    public function __construct($name = null, $options = array())
    {
        parent::__construct('oferta_szukaj');

        $this->setAttribute('method', 'get');
        $this->add(array(
            'name' => 'typ_oferty',
            'type' => 'Select',
            'options' => array(
                'label' => 'Typ oferty',
                'empty_option' => '-',
                'value_options' => array(
                    'S' => 'sprzedaż',
                    'W' => 'wynajem'
                )
            ),
        ));
        $this->add(array(
            'name' => 'typ_nieruchomosci',
            'type' => 'Select',
            'options' => array(
                'label' => 'Typ nieruchomości',
                'empty_option' => '-',
                'value_options' => array(
                    'M' => 'mieszkanie',
                    'D' => 'dom',
                    'G' => 'grunt'
                )
            ),
        ));
        $this->add(array(
            'name' => 'numer',
            'type' => 'Text',
            'options' => array(
                'label' => 'Numer',
            ),
        ));
        $this->add(array(
            'name' => 'cena_od',
            'type' => 'Text',
            'options' => array(
                'label' => 'Cena od',
            ),
        ));
        $this->add(array(
            'name' => 'cena_do',
            'type' => 'Text',
            'options' => array(
                'label' => 'Cena do',
            ),
        ));
        $this->add(array(
            'name' => 'cena',
            'type' => 'Text',
            'options' => array(
                'label' => 'Cena',
            ),
        ));
        $this->add(array(
            'name' => 'powierzchnia_od',
            'type' => 'Text',
            'options' => array(
                'label' => 'Powierzchnia od',
            ),
        ));
        $this->add(array(
            'name' => 'powierzchnia_do',
            'type' => 'Text',
            'options' => array(
                'label' => 'Powierzchnia do',
            ),
        ));
        $this->add(array(
            'name' => 'powierzchnia',
            'type' => 'Text',
            'options' => array(
                'label' => 'Powierzchnia',
            ),
        ));
        $this->add(array(
            'name' => 'szukaj',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Szukaj',
                'class' => 'btn btn-default'
            ),
        ));
    }
}