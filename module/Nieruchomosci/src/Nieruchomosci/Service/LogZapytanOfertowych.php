<?php
namespace Nieruchomosci\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter as DbAdapter;

class LogZapytanOfertowych implements DbAdapter\AdapterAwareInterface
{
    use DbAdapter\AdapterAwareTrait;

    public function __construct( $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function zapiszZapytanie($params)
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $insert = $sql->insert('log_zapytan_ofertowych');
        $insert->values(array(
            'email' => $params['email'],
            'telefon' => $params['telefon'],
            'tresc' => $params['tresc']
        ));

        $selectString = $sql->getSqlStringForSqlObject($insert);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

    }
}