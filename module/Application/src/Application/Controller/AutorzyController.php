<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Description of AutorzyController
 *
 * @author damian
 */
class AutorzyController extends AbstractActionController
{

    private $autor;

    public function __construct(\Application\Model\Autor $autor)
    {
        $this->autor = $autor;
    }

    public function listaAction()
    {
        return new ViewModel(array(
            'autorzy' => $this->autor->pobierzWszystko()
        ));

    }

    public function dodajAction()
    {
        $form = $this->getServiceLocator()->get('AutorForm');
        $form->get('zapisz')->setValue('Dodaj');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($this->autor->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->autor->dodaj($request->getPost());

                return $this->redirect()->toRoute('autorzy');
            }
        }
        return new ViewModel(array('tytul' => 'Dodawanie autora', 'form' => $form));
    }

    public function edytujAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id))
            $this->redirect()->toRoute('autorzy');

        $form = $this->getServiceLocator()->get('AutorForm');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($this->autor->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->autor->aktualizuj($id, $request->getPost());

                return $this->redirect()->toRoute('autorzy');
            }
        } else {
            $daneAutora = $this->autor->pobierz($id);
            $form->setData($daneAutora);
        }

        $viewModel = new ViewModel(array('tytul' => 'Edytuj autora', 'form' => $form));
        $viewModel->setTemplate('application/autorzy/dodaj');
        return $viewModel;
    }

    public function szczegolyAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id))
            $this->redirect()->toRoute('autorzy');

        $daneAutora = $this->autor->pobierz($id);
        $viewModel = new ViewModel(array('tytul' => 'Szczegóły autora', 'daneAutora' => $daneAutora));
        return $viewModel;
    }

    public function usunAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (empty($id))
            $this->redirect()->toRoute('autorzy');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'Nie');

            if ($del == 'Tak') {
                $id = (int) $request->getPost('id');
                $this->autor->usun($id);
            }

            return $this->redirect()->toRoute('autorzy');
        }

        return array(
            'id'    => $id,
            'daneAutora' => $this->autor->pobierz($id)
        );
    }

}
