<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Description of KsiazkiController
 *
 * @author damian
 */
class KsiazkiController extends AbstractActionController
{

    private $ksiazka;

    public function __construct(\Application\Model\Ksiazka $ksiazka)
    {
        $this->ksiazka = $ksiazka;
    }

    public function listaAction()
    {
        return new ViewModel(array(
            'ksiazki' => $this->ksiazka->pobierzWszystko()
        ));

    }

    public function dodajAction()
    {
        $form = $this->getServiceLocator()->get('KsiazkaForm');
        $form->get('zapisz')->setValue('Dodaj');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($this->ksiazka->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->ksiazka->dodaj($request->getPost());

                return $this->redirect()->toRoute('ksiazki');
            }
        }
        return new ViewModel(array('tytul' => 'Dodawanie książki', 'form' => $form));
    }

    public function edytujAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id))
            $this->redirect()->toRoute('ksiazki');

        $form = $this->getServiceLocator()->get('KsiazkaForm');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($this->ksiazka->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->ksiazka->aktualizuj($id, $request->getPost());

                return $this->redirect()->toRoute('ksiazki');
            }
        } else {
            $daneKsiazki = $this->ksiazka->pobierz($id);
            $form->setData($daneKsiazki);
        }

        $viewModel = new ViewModel(array('tytul' => 'Edytuj książkę', 'form' => $form));
        $viewModel->setTemplate('application/ksiazki/dodaj');
        return $viewModel;
    }

    public function szczegolyAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id))
            $this->redirect()->toRoute('ksiazki');

        $daneKsiazki = $this->ksiazka->pobierz($id);
        $viewModel = new ViewModel(array('tytul' => 'Szczegóły książki', 'daneKsiazki' => $daneKsiazki));
        return $viewModel;
    }

    public function usunAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id))
            $this->redirect()->toRoute('ksiazki');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'Nie');

            if ($del == 'Tak') {
                $id = (int) $request->getPost('id');
                $this->ksiazka->usun($id);;
            }

            return $this->redirect()->toRoute('ksiazki');
        }

        return array(
            'id'    => $id,
            'daneKsiazki' => $this->ksiazka->pobierz($id)
        );
    }

}
