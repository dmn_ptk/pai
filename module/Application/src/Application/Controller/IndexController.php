<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Application\Model as Model;

class IndexController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel();
    }

    public function dataAction() {
        $data = new Model\Data();

        return new ViewModel(array(
            'dzisiaj' => $data->dzisiaj(),
            'dni_tygodnia' => $data->dniTygodnia()
        ));
    }
    
    public function miesiaceAction(){
//        $miesiace = new \Application\Model\Miesiace();
        $miesiace = new Model\Miesiace();
        
        return new ViewModel(array(
           'wszystkieMiesiace' => $miesiace->pobierzWszystkie()
        ));
    }
    
    public function liczbyAction(){
        $liczby = new Model\Liczby();
        
        return new ViewModel(array(
           "liczby" => $liczby->generuj() 
        ));
    }

}
