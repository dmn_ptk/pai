<?php

namespace Application\Form;

use Zend\Form\Form;

class AutorForm extends Form {

    public function __construct() {
        parent::__construct('autor');

        $this->setAttributes(array('method' => 'post', 'class' => 'form'));
        $this->add(array(
            'name' => 'imie',
            'type' => 'Text',
            'options' => array(
                'label' => 'Imię',
            ),
            'attributes' => array('class' => 'form-control')
        ));
        $this->add(array(
            'name' => 'nazwisko',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nazwisko',
            ),
            'attributes' => array('class' => 'form-control')
        ));
        $this->add(array(
            'name' => 'zapisz',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Zapisz',
                'class' => 'btn btn-default'
            ),
        ));
    }

}