<?php

namespace Application\Form;

use Zend\Form\Form;

class KsiazkaForm extends Form {

    public function __construct(\Application\Model\Autor $autor) {
        parent::__construct('ksiazka');

        $this->setAttributes(array('method' => 'post', 'class' => 'form'));
        $this->add(array(
            'name' => 'tytul',
            'type' => 'Text',
            'options' => array(
                'label' => 'Tytuł',
            ),
            'attributes' => array('class' => 'form-control')
        ));
        $this->add(array(
            'name' => 'id_autora',
            'type' => 'Select',
            'options' => array(
                'label' => 'Autor',
                'value_options' => $autor->pobierzSlownik()
            ),
            'attributes' => array('class' => 'form-control')
        ));
        $this->add(array(
            'name' => 'cena',
            'type' => 'Text',
            'options' => array(
                'label' => 'Cena'
            ),
            'attributes' => array('class' => 'form-control')
        ));
        $this->add(array(
            'name' => 'liczba_stron',
            'type' => 'Text',
            'options' => array(
                'label' => 'Liczba stron',
            ),
            'attributes' => array('class' => 'form-control')
        ));
        $this->add(array(
            'name' => 'opis',
            'type' => 'Textarea',
            'options' => array(
                'label' => 'Opis',
            ),
            'attributes' => array('class' => 'form-control')
        ));
        $this->add(array(
            'name' => 'zapisz',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Zapisz',
                'class' => 'btn btn-default'
            ),
        ));
    }

}
