<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data
 *
 * @author damian
 */
namespace Application\Model; 
 
class Data 
{ 
  public function dzisiaj() 
  { 
    return date('Y-m-d H:i:s'); 
  } 
   
  public function dniTygodnia() 
  { 
    return array('Poniedziałek', 'Wtorek', 'Środa',  
      'Czwartek', 'Piątek', 'Sobota', 'Niedziela'); 
  } 
} 