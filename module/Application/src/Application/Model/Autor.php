<?php

namespace Application\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter as DbAdapter;
use Zend\I18n\Validator\IsFloat;

class Autor implements InputFilterAwareInterface, DbAdapter\AdapterAwareInterface
{
	use DbAdapter\AdapterAwareTrait;
    protected $inputFilter;
	
	public function pobierzSlownik()
	{
		$dbAdapter = $this->adapter;
		
		$sql = new Sql($dbAdapter);
		$select = $sql->select('autorzy');
		$select->order('nazwisko');
		
		$selectString = $sql->getSqlStringForSqlObject($select);
		$wyniki = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
		
		$temp = array();
		foreach($wyniki as $rek)
			$temp[$rek->id] = $rek->imie . ' ' . $rek->nazwisko;
		
		return $temp;
	}

    public function getInputFilter()
    {
        if(!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'nazwisko',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'imie',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(\Zend\InputFilter\InputFilterInterface $inputFilter)
    {

    }

    public function dodaj($dane)
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $insert = $sql->insert('autorzy');
        $insert->values(array(
            'imie' => $dane->imie,
            'nazwisko' => $dane->nazwisko,
        ));

        $selectString = $sql->getSqlStringForSqlObject($insert);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        try {
            return $wynik->getGeneratedValue();
        } catch(Exception $e) {
            return false;
        }
    }

    public function pobierzWszystko()
    {
        $dbAdapter = $this->adapter;
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from(array('a' => 'autorzy'));
        $select->order('a.id');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        return $wynik;
    }

    public function pobierz($id)
    {
        $dbAdapter = $this->adapter;
        $sql = new Sql($dbAdapter);
        $select = $sql->select('autorzy');
        $select->where(array('autorzy.id' => $id));

        $selectString = $sql->getSqlStringForSqlObject($select);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        if($wynik->count())
            return $wynik->current();
        else
            return array();
    }

    public function aktualizuj($id, $dane)
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $update = $sql->update('autorzy');
        $update->set(array(
            'nazwisko' => $dane->nazwisko,
            'imie' => $dane->imie,
        ));
        $update->where(array('id' => $id));

        $selectString = $sql->getSqlStringForSqlObject($update);
        $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        return true;
    }

    public function usun($id)
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $delete = $sql->delete('autorzy');
        $delete->where(array('id' => $id));

        $selectString = $sql->getSqlStringForSqlObject($delete);
        $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        return true;
    }
}