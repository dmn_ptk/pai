<?php

namespace Application\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter as DbAdapter;
use Zend\I18n\Validator\IsFloat ;

class Ksiazka implements InputFilterAwareInterface, DbAdapter\AdapterAwareInterface
{
    use DbAdapter\AdapterAwareTrait;

    protected $inputFilter;

    public function getInputFilter()
    {
        if(!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'tytul',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id_autora',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'cena',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new IsFloat(array('locale' => 'en')),
                    array('name' => 'GreaterThan', 'options' => array('min' => 0)),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'liczba_stron',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array('name' => 'Digits'),
                    array('name' => 'GreaterThan', 'options' => array('min' => 0)),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'opis',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(\Zend\InputFilter\InputFilterInterface $inputFilter)
    {

    }

    public function dodaj($dane)
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $insert = $sql->insert('ksiazki');
        $insert->values(array(
            'id_autora' => $dane->id_autora,
            'tytul' => $dane->tytul,
            'opis' => $dane->opis,
            'cena' => $dane->cena,
            'liczba_stron' => $dane->liczba_stron,
        ));

        $selectString = $sql->getSqlStringForSqlObject($insert);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        try {
            return $wynik->getGeneratedValue();
        } catch(Exception $e) {
            return false;
        }
    }

    public function pobierzWszystko()
    {
        $dbAdapter = $this->adapter;
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from(array('k' => 'ksiazki'));
        $select->join(array('a' => 'autorzy'), 'k.id_autora = a.id', array('imie', 'nazwisko'));
        $select->order('k.id');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        return $wynik;
    }

    public function pobierz($id)
    {
        $dbAdapter = $this->adapter;
        $sql = new Sql($dbAdapter);
        $select = $sql->select('ksiazki');
        $select->join(array('a' => 'autorzy'), 'a.id=ksiazki.id_autora');
        $select->where(array('ksiazki.id' => $id));
        $select->order('tytul');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $wynik = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        if($wynik->count())
            return $wynik->current();
        else
            return array();
    }

    public function aktualizuj($id, $dane)
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $update = $sql->update('ksiazki');
        $update->set(array(
            'id_autora' => $dane->id_autora,
            'tytul' => $dane->tytul,
            'opis' => $dane->opis,
            'cena' => $dane->cena,
            'liczba_stron' => $dane->liczba_stron,
        ));
        $update->where(array('id' => $id));

        $selectString = $sql->getSqlStringForSqlObject($update);
        $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        return true;
    }

    public function usun($id)
    {
        $dbAdapter = $this->adapter;

        $sql = new Sql($dbAdapter);
        $delete = $sql->delete('ksiazki');
        $delete->where(array('id' => $id));

        $selectString = $sql->getSqlStringForSqlObject($delete);
        $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);

        return true;
    }
}