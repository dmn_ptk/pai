<?php

/**
 * Description of Liczby
 *
 * @author damian
 */

namespace Application\Model; 
class Liczby {
    public function generuj(){
        $liczby = array(
            'parzyste' => array(),
            'nieparzyste' => array()
        );
        
        for($i=100; --$i>=0;){
            $liczba = rand(0,100);
            
            if($liczba%2)
                $liczby['nieparzyste'][] = $liczba;
            else
                $liczby['parzyste'][] = $liczba;
        }
        
        sort($liczby['parzyste']);
        sort($liczby['nieparzyste']);
        
        return $liczby;
    }
    
}
