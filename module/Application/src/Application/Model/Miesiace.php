<?php

/**
 * Description of Miesiace
 *
 * @author damian
 */

namespace Application\Model; 

class Miesiace {
    public function pobierzWszystkie(){
        return array(
            'red' => 'Styczeń',
            'cyan' => 'Luty',
            'blue' => 'Marzec',
            'darkblue' => 'Kwiecień',
            'lightblue' => 'Maj',
            'purple' => 'Czerwiec',
            'yellow' => 'Lipiec',
            'lime' => 'Sierpień',
            'magenta' => 'Wrzesień',
            'silver' => 'Październik',
            'orange' => 'Listopad',
            'brown' => 'Grudzień'
        );
    }
}
